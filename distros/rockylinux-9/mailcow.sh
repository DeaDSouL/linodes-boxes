#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Bootstrap:
declare -a SCRIPT=("$(realpath "$0" 2>/dev/null)" "$(realpath "${BASH_SOURCE[0]}" 2>/dev/null)")
[ ! -f "${SCRIPT[0]}" ] && SCRIPT[0]=${SCRIPT[1]}
DISTRO=$(basename $(dirname "${SCRIPT[0]}"))
BASE_PATH="${BASE_PATH:-$(dirname $(dirname $(dirname "${SCRIPT[0]}")))}"
if [ -f "${BASE_PATH}/bootstrap.sh" ]; then source "${BASE_PATH}/bootstrap.sh"
else echo -e 'Could not import "bootstrap.sh"\nAborting..'; sleep 5; exit 1; fi

inc helper/permission
require_root_or_sudo


# Essentials
deploy_preset 'essentials'

# Docker
deploy_preset 'docker'


# Add NTP pools
echo -e '\n[Time]\nNTP=0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org 3.pool.ntp.org\n' >> /etc/systemd/timesyncd.conf

# Timezone
timedatectl set-timezone Asia/Kuwait

# Firewall
firewall-cmd --permanent --zone=public \
    --add-service=smtp \
    --add-service=smtps \
    --add-service=smtp-submission \
    --add-service=imap \
    --add-service=imaps \
    --add-service=pop3 \
    --add-service=pop3s \
    --add-service=managesieve \
    --add-service=http \
    --add-service=https
firewall-cmd --reload

# MailCow
cd /opt
git clone https://github.com/mailcow/mailcow-dockerized.git
cd mailcow-dockerized && ./generate_config.sh
docker compose pull
docker compose up -d

