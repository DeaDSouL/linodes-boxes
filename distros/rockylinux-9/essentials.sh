#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Bootstrap:
declare -a SCRIPT=("$(realpath "$0" 2>/dev/null)" "$(realpath "${BASH_SOURCE[0]}" 2>/dev/null)")
[ ! -f "${SCRIPT[0]}" ] && SCRIPT[0]=${SCRIPT[1]}
DISTRO=$(basename $(dirname "${SCRIPT[0]}"))
BASE_PATH="${BASE_PATH:-$(dirname $(dirname $(dirname "${SCRIPT[0]}")))}"
if [ -f "${BASE_PATH}/bootstrap.sh" ]; then source "${BASE_PATH}/bootstrap.sh"
else echo -e 'Could not import "bootstrap.sh"\nAborting..'; sleep 5; exit 1; fi

inc helper/permission
require_root_or_sudo


# DNF
dnf --assumeyes install dnf-plugins-core

# RPMFusion:
dnf --assumeyes install --nogpgcheck https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(rpm -E %rhel).noarch.rpm
dnf --assumeyes install --nogpgcheck \
    https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm \
    https://mirrors.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %rhel).noarch.rpm

# Good Utils
dnf --assumeyes install \
    git \
    vim-enhanced \
    htop \
    tmux

# Configure tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
cp "${BASE_PATH}/shared/tmux.conf" ~/.tmux/
ln -s ~/.tmux/tmux.conf ~/.tmux.conf

