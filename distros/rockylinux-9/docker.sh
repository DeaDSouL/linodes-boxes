#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Bootstrap:
declare -a SCRIPT=("$(realpath "$0" 2>/dev/null)" "$(realpath "${BASH_SOURCE[0]}" 2>/dev/null)")
[ ! -f "${SCRIPT[0]}" ] && SCRIPT[0]=${SCRIPT[1]}
DISTRO=$(basename $(dirname "${SCRIPT[0]}"))
BASE_PATH="${BASE_PATH:-$(dirname $(dirname $(dirname "${SCRIPT[0]}")))}"
if [ -f "${BASE_PATH}/bootstrap.sh" ]; then source "${BASE_PATH}/bootstrap.sh"
else echo -e 'Could not import "bootstrap.sh"\nAborting..'; sleep 5; exit 1; fi

inc helper/permission
require_root_or_sudo


# Docker
dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

dnf --assumeyes --refresh upgrade

dnf --assumeyes install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo -e '\n{\n\t"selinux-enabled": true\n}\n' >> /etc/docker/daemon.json

systemctl enable --now docker

#docker info | grep selinux

#rpm -qa | grep container-selinux

