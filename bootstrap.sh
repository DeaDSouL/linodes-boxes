#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------

# 0: if it was executed,    1: if it was sourced
declare -a SCRIPT=("$(realpath "$0" 2>/dev/null)" "$(realpath "${BASH_SOURCE[0]}" 2>/dev/null)")
[ ! -f "${SCRIPT[0]}" ] && SCRIPT[0]=${SCRIPT[1]}
BASE_PATH="${BASE_PATH:-$(dirname "${SCRIPT[0]}")}"
unset SCRIPT


# START_DTIME_HUMAN
START_DTIME_HUMAN="${REAL_START_DTIME_HUMAN:-$(date +'%Y/%m/%d %H:%M:%S')}"
# START_DTIME
START_DTIME="${REAL_START_DTIME:-$(date -d "$START_DTIME_HUMAN" +'%Y%m%d_%H%M%S')}"


function _bash() { #{{{
    # Doc: #{{{
    # About:
    #   Execute bash scripts which are located inside 'linode-boxes'
    #   with the needed environmental variables
    # Params:
    #   1:  script file that is located in the current distro folder
    #       we're in, or somewhere inside the base of linode-boxes folder
    # Example:
    #   _bash 'rockylinux/_essentials.sh'
    #   _bash '_essentials.sh'
    #}}}
    local bash_file=''

    if [ -f "${BASE_PATH}/${DISTRO}/$1" ]; then
        bash_file="${BASE_PATH}/${DISTRO}/$1"
    elif [ -f "${BASE_PATH}/$1" ]; then
        bash_file="${BASE_PATH}/$1"
    fi

    if [ ! -z "${bash_file}" ]; then
        env \
            REAL_START_DTIME="${START_DTIME}" \
            REAL_START_DTIME_HUMAN="${START_DTIME_HUMAN}" \
            bash "${bash_file}"
    fi
}
#}}}
function deploy_preset() { #{{{
    # Doc: #{{{
    # About:
    #   Execute preset scripts which are located inside 'distros/DISTRO_NAME/'
    #   with the needed environmental variables
    # Params:
    #   1:  script file that is located in the current distro folder
    #       we're in, or somewhere inside distros/ folder or even
    #       somewhere inside the base of linode-boxes folder
    # Example:
    #   1)
    #       deploy_preset 'test'
    #   2)
    #       deploy_preset 'rockylinux-9/test.sh'
    #   3)
    #       deploy_preset 'distros/rockylinux-9/test'
    #}}}
    local bash_file="$1"

    [[ ! "$bash_file" =~ \.sh$ ]] && bash_file="${bash_file}.sh"

    # ex: deploy_preset 'mailcow'
    if [[ ! -z ${DISTRO} && -f "${BASE_PATH}/distros/${DISTRO}/${bash_file}" ]]; then
        bash_file="${BASE_PATH}/distros/${DISTRO}/${bash_file}"
    # ex: deploy_preset 'rockylinux-9/mailcow'
    elif [ -f "${BASE_PATH}/distros/${bash_file}" ]; then
        bash_file="${BASE_PATH}/distros/${bash_file}"
    # ex: deploy_preset 'distros/rockylinux-9/mailcow'
    elif [ -f "${BASE_PATH}/${bash_file}" ]; then
        bash_file="${BASE_PATH}/${bash_file}"
    else
        bash_file=''
    fi

    if [ ! -z "${bash_file}" ]; then
        env \
            REAL_START_DTIME="${START_DTIME}" \
            REAL_START_DTIME_HUMAN="${START_DTIME_HUMAN}" \
            bash "${bash_file}"
    fi
}
#}}}
function inc() { #{{{
    # Doc: #{{{
    # About:
    #   Source bash scripts which are located inside 'lib' or 'helper'
    #   or even if we get a fullpath like: '/some/path/to/some/file'
    # Params:
    #   1:  bash file that is located in the current distro folder
    #       we're in, or somewhere inside the base of linode-boxes folder
    # Example:
    #   1)
    #       inc lib/colors
    #   2)
    #       inc 'helper/checks'
    #   3)
    #       inc ~/.bashrc
    #}}}
    local bash_file="$1" type=''
    type=${bash_file#/} # Remove leading '/'
    type=${type%%/*}    # Extract the first directory

    [[ ! "$bash_file" =~ \.sh$ ]] && bash_file="${bash_file}.sh"

    # starts with a slash '/' or '~/'
    if [[ "${bash_file:0:1}" == '/' || "${bash_file:0:2}" == '~/' ]]; then
        [ -f "${bash_file}" ] || bash_file=''
    # starts with 'lib' or 'helper'
    elif [[ ("${type}" == 'lib' || "${type}" == 'helper') \
        && -f "${BASE_PATH}/${bash_file}" ]]; then
        bash_file="${BASE_PATH}/${bash_file}"
    else
        bash_file=''
    fi

    if [ ! -z "${bash_file}" ]; then
        source "${bash_file}"
    else
        :
        #_log_error "Can not import '$1'"
    fi
}
#}}}

