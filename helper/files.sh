#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Renaming #{{{
# Returns:
#   0: if "$1" was renamed
#   1: if "$1" was not renamed
#   2: if "$1" does not exist
#   3: if "$1" is not a file
#   4: if "$1" is not a directory
#   5: if "$1" is not a link

function auto_rename() { #{{{
    # Doc: #{{{
    # About:
    #   Auto rename the given file/directory
    # Params:
    #   1: file or directory
    # Example:
    #   auto_rename ~/some_file_or_dir
    # Return:
    #   0: if "$1" was renamed
    #   1: if "$1" wasn't renamed
    #}}}
    local DTIME="$(date +'%Y%m%d_%H%M%S')"

    # if the new name is taken, suffix it with current nanoseconds
    [ -e "${1}.bkp-${DTIME}" ] && DTIME="${DTIME}.$(date +'%N')"

    mv -Zv "$1"{,.bkp-${DTIME}} 1>/dev/null 2>&1 && {
        #_log_debug 'success'
        return 0
    }
    #_log_debug "failed renaming: '$1'"
    return 1

}
#}}}
function rename_if_is_exist() { #{{{
    # Doc: #{{{
    # About:
    #   Auto rename the given file/directory if it exists
    # Params:
    #   1: file or directory
    # Example:
    #   rename_if_is_exist ~/bin
    # Return:
    #   0: if "$1" was renamed
    #   1: if "$1" wasn't renamed
    #   2: if "$1" does not exist
    #}}}
    [ -e "$1" ] && return $(auto_rename "$1")
    #_log_debug "does not exist: '$1'"
    return 2
}
#}}}
function rename_if_is_file() { #{{{
    # Doc: #{{{
    # About:
    #   Auto rename the given file if it is a file
    # Params:
    #   1: file or directory
    # Example:
    #   rename_if_is_file ~/some_file
    # Return:
    #   0: if "$1" was renamed
    #   1: if "$1" wasn't renamed
    #   3: if "$1" is not a file
    #}}}
    [ -f "$1" ] && return $(auto_rename "$1")
    #_log_debug "is not a file: '$1'"
    return 3
}
#}}}
function rename_if_is_dir() { #{{{
    # Doc: #{{{
    # About:
    #   Auto rename the given file if it is a directory
    # Params:
    #   1: file or directory
    # Example:
    #   rename_if_is_dir ~/some_dir
    # Return:
    #   0: if "$1" was renamed
    #   1: if "$1" wasn't renamed
    #   4: if "$1" is not a directory
    #}}}
    [ -d "$1" ] && return $(auto_rename "$1")
    #_log_debug "is not a directory: '$1'"
    return 4
}
#}}}
function rename_if_is_link() { #{{{
    # Doc: #{{{
    # About:
    #   Auto rename the given file/directory if it is a link
    # Params:
    #   1: file or directory
    # Example:
    #   rename_if_is_link ~/some_link
    # Return:
    #   0: if "$1" was renamed
    #   1: if "$1" wasn't renamed
    #   5: if "$1" is not a link
    #}}}
    [ -L "$1" ] && return $(auto_rename "$1")
    #_log_debug "is not a link: '$1'"
    return 5
}
#}}}
#}}}

function get_extension() { #{{{
    # Doc: #{{{
    # About:
    #   Echo the extension without the dot "."
    # Params:
    #   1: file or directory
    # Example:
    #   auto_rename ~/foo.bar
    # Return:
    #   0: if "$1" has an extension, and we succeed in getting it
    #   1: if "$1" is empty or doesn't have an extension
    #}}}
    local extension=${1##*.}
    [[ "$1" != "$extension" && ! -z "$extension" ]] && {
        echo "$extension"
        return 0
    }
    return 1
}
#}}}
function get_filename() { #{{{
    # Doc: #{{{
    # About:
    #   Echo the filename without the extension
    # Params:
    #   1: file or directory
    # Example:
    #   get_filename ~/foo.bar
    # Return:
    #   0: if "$1" has an extension, and we succeed in getting the filename
    #   1: if "$1" is empty
    #   2: if "$1" is a dotfile (starts with .) whether or not it has an extension
    #   3: if "$1" doesn't have an extension
    #}}}
    local filename="$1" prefix ret
    [[ "$filename" == .* ]] && {
        filename=${filename:1}
        prefix='.'
        ret=2
    }

    if [[ -z "$filename" ]]; then
        filename=''
        ret=1
    elif [[ "$filename" == *.* ]]; then
        if get_extension "$filename" >/dev/null; then
            filename=${filename%.*}
            [[ ! -z "$filename" ]] && ret=${ret:-0}
        fi
    fi

    echo "${prefix}${filename}"
    return ${ret:-3}
}
#}}}


function read_ini() { #{{{
    # Doc: #{{{
    # About:
    #   Loads a given ini file into an associative array
    #   indexed by section.key
    # Params:
    #   1: ini file
    # Example:
    #   auto_rename ~/foo.bar
    #   1)
    #       # get a single value
    #       declare -A assoc="($(read_ini settings.ini))"
    #       echo assoc[Settings.some_key]
    #   2)
    #       # loop through it
    #       declare -A sett="($(read_ini settings.ini))"
    #       for k in "${!sett[@]}"; do
    #           v="${sett[$k]}"
    #           echo "$k : $v"
    #       done
    #
    # Return:
    #   0:              if succeeded in reading the given ini file
    #   1:              if there is no given ini file
    #   2:              if the given ini file does not exist
    #   3:              if the given ini file doesn't have data
    #   4:              if the given ini file is not readable
    #}}}
    local \
        line \
        current_section \
        key \
        value \
        ini_file="$1"

    [ -z "$1" ] && return 1             # If there is no given ini file
    [ ! -f "$ini_file" ] && return 2    # If the given ini file does not exist
    [ ! -s "$ini_file" ] && return 3    # If the given ini file doesn't have data
    [ ! -r "$ini_file" ] && return 4    # If the given ini file is not readable

    declare -A ini_values=()

    while IFS= read -r line || [[ -n "$line" ]]; do
        # Skip empty lines and comments
        [[ "$line" =~ ^\; ]] && continue
        [[ "$line" =~ ^\# ]] && continue
        [[ -z "$line" ]] && continue

        # Parse sections
        if [[ "$line" =~ ^\[.*\]$ ]]; then
            current_section="${line#[}"
            current_section="${current_section%]}"
            continue
        fi

        # Parse key-value pairs
        key="${line%%=*}"
        value="${line#*=}"

        # Store the values in an associative array
        ini_values["$current_section.$key"]=$value
    done < "$ini_file"

    # If the given ini file doesn't have data
    [ "${#ini_values[@]}" -eq 0 ] && { unset ini_values; return 3; }

    # Return an expand associative array as string.
    echo "${ini_values[@]@K}"

    unset ini_values
    return 0
}
#}}}


# Aliases as functions:
function get_ext() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: get_extension()
    #   Check it with: docs_in helper/files get_extension
    #}}}
    return $(get_extension "$1")
}
#}}}
