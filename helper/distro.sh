#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


function get_distro() { #{{{
    # Doc: #{{{
    # About:
    #   Echo the distro ID, name, version, or version ID
    # Params:
    #   1: string id|name|ver|ver_id
    # Example:
    #   1)
    #       get_distro id
    #   2)
    #       get_distro ver_id
    # Return:
    #   0: if found the requested info
    #   1: if failed to find the requested info
    #}}}
    local OSRELEASE='/etc/os-release' OUTPUT

    case "$1" in
        id)
            OUTPUT=$(grep ^ID= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
            ;;
        name)
            OUTPUT=$(grep ^NAME= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
            ;;
        ver_id|verid|version_id|versionid)
            OUTPUT=$(grep ^VERSION_ID= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
            ;;
        ver|version)
            OUTPUT=$(grep ^VERSION= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
            ;;
        *)
            ;;
    esac

    # Remove '"' if the name has it
    OUTPUT="${OUTPUT//\"/}"

    [ ! -z "${OUTPUT}" ] && {
        echo "${OUTPUT}"
        return 0
    }
    # _log_debug 'could not know the requested info about the distro we are on!'
    return 1
}
#}}}

function distro_is_rhel() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on RHEL
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_rhel
    #   2)
    #       if distro_is_rhel; then
    #           echo 'we are on RHEL'
    #       else
    #           echo 'we are not on RHEL'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == rhel ]] && return 0
    # _log_debug 'The Distro we are on is not RHEL!'
    return 1
}
#}}}
function distro_is_rocky() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Rocky Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_rocky
    #   2)
    #       if distro_is_rocky; then
    #           echo 'we are on RockyLinux'
    #       else
    #           echo 'we are not on RockyLinux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == rocky ]] && return 0
    # _log_debug 'The Distro we are on is not Rocky!'
    return 1
}
#}}}
function distro_is_almalinux() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on AlmaLinux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_almalinux
    #   2)
    #       if distro_is_almalinux; then
    #           echo 'we are on AlmaLinux'
    #       else
    #           echo 'we are not on AlmaLinux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == almalinux ]] && return 0
    # _log_debug 'The Distro we are on is not AlmaLinux!'
    return 1
}
#}}}
function distro_is_opensuse() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on OpenSUSE
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_opensuse
    #   2)
    #       if distro_is_opensuse; then
    #           echo 'we are on OpenSUSE'
    #       else
    #           echo 'we are not on OpenSUSE'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == opensuse-leap || $(get_distro id) == opensuse-tumbleweed ]] && return 0
    # _log_debug 'The Distro we are on is not OpenSUSE!'
    return 1
}
#}}}
function distro_is_centos() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on CentOS Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_centos
    #   2)
    #       if distro_is_centos; then
    #           echo 'we are on CentOS Linux'
    #       else
    #           echo 'we are not on CentOS Linux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == centos ]] && return 0
    # _log_debug 'The Distro we are on is not CentOS Linux'
    return 1
}
#}}}
function distro_is_scientific() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Scientific Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_scientific
    #   2)
    #       if distro_is_scientific; then
    #           echo 'we are on Scientific Linux'
    #       else
    #           echo 'we are not on Scientific Linux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == scientific ]] && return 0
    # _log_debug 'The Distro we are on is not Scientific Linux'
    return 1
}
#}}}
function distro_is_fedora() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Fedora
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_fedora
    #   2)
    #       if distro_is_fedora; then
    #           echo 'we are on Fedora'
    #       else
    #           echo 'we are not on Fedora'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == fedora ]] && return 0
    # _log_debug 'The Distro we are on is not Fedora!'
    return 1
}
#}}}
function distro_is_debian() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Debian GNU/Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_debian
    #   2)
    #       if distro_is_debian; then
    #           echo 'we are on Debian GNU/Linux'
    #       else
    #           echo 'we are not on Debian GNU/Linux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == debian ]] && return 0
    # _log_debug 'The Distro we are on is not Debian GNU/Linux!'
    return 1
}
#}}}
function distro_is_ubuntu() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Ubuntu
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_ubuntu
    #   2)
    #       if distro_is_ubuntu; then
    #           echo 'we are on Ubuntu'
    #       else
    #           echo 'we are not on Ubuntu'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == ubuntu ]] && return 0
    # _log_debug 'The Distro we are on is not Ubuntu'
    return 1
}
#}}}
function distro_is_linuxmint() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on LinuxMint
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_linuxmint
    #   2)
    #       if distro_is_linuxmint; then
    #           echo 'we are on LinuxMint'
    #       else
    #           echo 'we are not on LinuxMint'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == linuxmint ]] && return 0
    # _log_debug 'The Distro we are on is not LinuxMint'
    return 1
}
#}}}
function distro_is_alpine() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Alpine Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_alpine
    #   2)
    #       if distro_is_alpine; then
    #           echo 'we are on Alpine Linux'
    #       else
    #           echo 'we are not on Alpine Linux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == alpine ]] && return 0
    # _log_debug 'The Distro we are on is not Alpine Linux!'
    return 1
}
#}}}
function distro_is_nixos() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on NixOS
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_nixos
    #   2)
    #       if distro_is_nixos; then
    #           echo 'we are on NixOS'
    #       else
    #           echo 'we are not on NixOS'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == nixos ]] && return 0
    # _log_debug 'The Distro we are on is not NixOS!'
    return 1
}
#}}}
function distro_is_gentoo() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Gentoo
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_gentoo
    #   2)
    #       if distro_is_gentoo; then
    #           echo 'we are on Gentoo'
    #       else
    #           echo 'we are not on Gentoo'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == gentoo ]] && return 0
    # _log_debug 'The Distro we are on is not Gentoo!'
    return 1
}
#}}}
function distro_is_void() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on VoidLinux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_void
    #   2)
    #       if distro_is_void; then
    #           echo 'we are on VoidLinux'
    #       else
    #           echo 'we are not on VoidLinux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    if $(grep 'Void' /etc/issue >/dev/null 2>&1); then
        return 0
    fi
    # _log_debug 'The Distro we are on is not VoidLinux!'
    return 1
}
#}}}
function distro_is_arch() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on ArchLinux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_arch
    #   2)
    #       if distro_is_arch; then
    #           echo 'we are on ArchLinux'
    #       else
    #           echo 'we are not on ArchLinux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == arch ]] && return 0
    # _log_debug 'The Distro we are on is not ArchLinux!'
    return 1
}
#}}}
function distro_is_manjaro() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not we are on Manjaro Linux
    # Params:
    #   N/A
    # Example:
    #   1)
    #       distro_is_manjaro
    #   2)
    #       if distro_is_manjaro; then
    #           echo 'we are on Manjaro Linux'
    #       else
    #           echo 'we are not on Manjaro Linux'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ $(get_distro id) == manjaro ]] && return 0
    # _log_debug 'The Distro we are on is not Manjaro Linux!'
    return 1
}
#}}}

# @TODO: handle:
#   1. if the distro doesn't have a version ID (such as rolling distros)
#   2. if the given input is not number
function distro_ver_gt() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not the current distro version id is
    #   greater than the given number
    # Params:
    #   1: number
    # Example:
    #   1)
    #       distro_ver_gt 9.2
    #   2)
    #       if $(distro_ver_gt 9.1); then
    #           echo 'Current distro version ID is greater than 9.1'
    #       else
    #           echo 'Current distro version ID is not greater than 9.1'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    local VER_ID=$(get_distro verid)
    if command_exists bc; then
        if (( $(echo "$1 < $VER_ID" | bc -l) )); then
            return 0
        fi
    else
        if (( $(awk 'BEGIN {print ("'"$1"'" < "'"$VER_ID"'")}') )); then
            return 0
        fi
    fi
    # _log_debug 'The given number is not greater than the current distro version ID!'
    return 1
}
#}}}
function distro_ver_ge() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not the current distro version id is
    #   greater than or equal the given number
    # Params:
    #   1: number
    # Example:
    #   1)
    #       distro_ver_ge 9.2
    #   2)
    #       if $(distro_ver_ge 9.1); then
    #           echo 'Current distro version ID is greater than or equal 9.1'
    #       else
    #           echo 'Current distro version ID is not greater than or equal 9.1'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    local VER_ID=$(get_distro verid)
    if command_exists bc; then
        if (( $(echo "$1 <= $VER_ID" | bc -l) )); then
            return 0
        fi
    else
        if (( $(awk 'BEGIN {print ("'"$1"'" <= "'"$VER_ID"'")}') )); then
            return 0
        fi
    fi
    # _log_debug 'The given number is not greater than or equal the current distro version ID!'
    return 1
}
#}}}
function distro_ver_lt() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not the current distro version id is
    #   less than the given number
    # Params:
    #   1: number
    # Example:
    #   1)
    #       distro_ver_lt 9.2
    #   2)
    #       if $(distro_ver_lt 9.1); then
    #           echo 'Current distro version ID is less than 9.1'
    #       else
    #           echo 'Current distro version ID is not less than 9.1'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    local VER_ID=$(get_distro verid)
    if command_exists bc; then
        if (( $(echo "$1 > $VER_ID" | bc -l) )); then
            return 0
        fi
    else
        if (( $(awk 'BEGIN {print ("'"$1"'" > "'"$VER_ID"'")}') )); then
            return 0
        fi
    fi
    # _log_debug 'The given number is not less than the current distro version ID!'
    return 1
}
#}}}
function distro_ver_le() { #{{{
    # Doc: #{{{
    # About:
    #   Check whether or not the current distro version id is
    #   less than or equal the given number
    # Params:
    #   1: number
    # Example:
    #   1)
    #       distro_ver_le 9.2
    #   2)
    #       if $(distro_ver_le 9.1); then
    #           echo 'Current distro version ID is less than or equal 9.1'
    #       else
    #           echo 'Current distro version ID is not less than or equal 9.1'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    local VER_ID=$(get_distro verid)
    if command_exists bc; then
        if (( $(echo "$1 >= $VER_ID" | bc -l) )); then
            return 0
        fi
    else
        if (( $(awk 'BEGIN {print ("'"$1"'" >= "'"$VER_ID"'")}') )); then
            return 0
        fi
    fi
    # _log_debug 'The given number is not less than or equal the current distro version ID!'
    return 1
}
#}}}


# Aliases
function distro_is_redhat() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: distro_is_rhel()
    #   Check it with: docs_in helper/distro distro_is_rhel
    #}}}
    return $(distro_is_rhel)
}
#}}}
function distro_is_alma() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: distro_is_almalinux()
    #   Check it with: docs_in helper/distro distro_is_almalinux
    #}}}
    return $(distro_is_almalinux)
}
#}}}
