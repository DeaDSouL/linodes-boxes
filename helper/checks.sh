#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Commands & Binaries
function command_exists() { #{{{
    # Doc: #{{{
    # About:
    #   Checks for existence of a command
    # Params:
    #   1: command to check
    #   2: (optional) log message to include when command not found
    # Example:
    #   command_exists ls && echo exists
    #}}}
    local msg="${2:-Command '$1' does not exist}"
    if type -t "$1" > /dev/null; then
        return 0
    else
        #_log_debug "$msg"
        return 1
    fi
}
#}}}
function binary_exists() { #{{{
    # Doc: #{{{
    # About:
    #   Checks for existence of a binary
    # Params:
    #   1: binary to check
    #   2: (optional) log message to include when binary not found
    # Example:
    #   binary_exists ls && echo exists
    #}}}
    local msg="${2:-Binary '$1' does not exist}"
    if type -P "$1" > /dev/null; then
        return 0
    else
        #_log_debug "$msg"
        return 1
    fi
}
#}}}


# Files & Directories
function is_exist() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #
    #   Note that everything in Linux is a file,
    #   even directories are files in Linux world.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       is_exist ~/.bashrc
    #   2)
    #       if is_exist ~/Downloads; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -e "$1" ] && return 0
    return 1
}
#}}}
function is_file() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and is a regular file
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_file ~/some_file
    #   2)
    #       if is_file ~/some_file; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -f "$1" ] && return 0
    return 1
}
#}}}
function is_directory() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and is directory
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_directory ~/some_dir
    #   2)
    #       if is_directory ~/some_dir; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -d "$1" ] && return 0
    return 1
}
#}}}
function is_symlink() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and is a symbolic link
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_symlink ~/some_symlink
    #   2)
    #       if is_symlink ~/some_symlink; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -L "$1" ] && return 0 # (same as -h)
    return 1
}
#}}}
function is_readable() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and the user has read access
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_readable ~/some_file
    #   2)
    #       if is_readable ~/some_folder; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -r "$1" ] && return 0
    return 1
}
#}}}
function is_writable() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and the user has write access
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_writable ~/some_file
    #   2)
    #       if is_writable ~/some_folder; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -w "$1" ] && return 0
    return 1
}
#}}}
function is_executable() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and the user has execute (or search) access
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       is_executable ~/some_file
    #   2)
    #       if is_executable ~/some_folder; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -x "$1" ] && return 0
    return 1
}
#}}}
function is_pipe() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and is a named pipe.
    #   (also known as a FIFO, or First In, First Out)
    # Params:
    #   1: file
    # Example:
    #   1)
    #       is_pipe ~/some_file
    #   2)
    #       if is_piep ~/some_file; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -p "$1" ] && return 0
    return 1
}
#}}}
function is_socket() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and is a socket
    # Params:
    #   1: file
    # Example:
    #   1)
    #       is_socket ~/some_file
    #   2)
    #       if is_socket ~/some_file; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -S "$1" ] && return 0
    return 1
}
#}}}
function is_block() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and is block special
    # Params:
    #   1: file
    # Example:
    #   1)
    #       is_block /dev/sda
    #   2)
    #       if is_block /dev/sdb; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -b "$1" ] && return 0
    return 1
}
#}}}
function is_char_special() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and is character special
    # Params:
    #   1: file
    # Example:
    #   1)
    #       is_block /path/to/file1
    #   2)
    #       if is_block /some/file2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -c "$1" ] && return 0
    return 1
}
#}}}
function is_fd() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file descriptor (FD)
    #   is opened on a terminal
    #
    #   Note:
    #       - The file descriptor is a mechanism used by
    #         Unix-like operating systems to refer to or handle
    #         input and output streams, among other things.
    #       - It's used to determine whether a script or program
    #         is being run interactively in a terminal or not.
    #         If a file descriptor (FD) is open on a terminal,
    #         the test returns true, indicating that the associated
    #         input or output is connected to a terminal.
    # Params:
    #   1: FD
    # Example:
    #   1)
    #       is_fd /path/to/file_descriptor
    #   2)
    #       if is_fd /path/to/file_descriptor; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -t "$1" ] && return 0
    return 1
}
#}}}

function is_newer_than() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the first given file/directory
    #   is newer than the second given file/directory
    # Params:
    #   1: file or directory
    #   2: file or directory
    # Example:
    #   1)
    #       is_newer_than ~/file1 ~/file2
    #   2)
    #       if is_newer_than ~/file1 ~/file2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ "$1" -nt "$2" ] && return 0
    return 1
}
#}}}
function is_older_than() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the first given file/directory
    #   is older than the second given file/directory
    # Params:
    #   1: file or directory
    #   2: file or directory
    # Example:
    #   1)
    #       is_older_than ~/file1 ~/file2
    #   2)
    #       if is_older_than ~/file1 ~/file2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ "$1" -ot "$2" ] && return 0
    return 1
}
#}}}

function has_data() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given file or directory exists
    #   and has a size greater than zero.
    # Params:
    #   1: file or directory
    # Example:
    #   1)
    #       has_data ~/some_file
    #   2)
    #       if has_data ~/some_dir; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -s "$1" ] && return 0
    return 1
}
#}}}
function has_sgid() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and has the set-group-ID (SGID) permission
    #
    #   Note:
    #       - It's set as `chmod g+s filename`.
    #       - When the SGID permission is set on an executable file,
    #         it means that when the file is executed, it runs with
    #         the group ID of the group that owns the file.
    #       - If the file is a script or an executable, it will
    #         inherit the group ownership of the directory it is in.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       has_sgid /usr/sbin/mount.cifs
    #   2)
    #       if has_sgid /usr/sbin/mount.cifs; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -g "$1" ] && return 0
    return 1
}
#}}}
function has_suid() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and has the set-user-ID (SUID) permission
    #
    #   Note:
    #       - It's set as `chmod +s filename`.
    #       - The set-user-ID (SUID) bit is a permission on a file
    #         that allows the program to be executed with the
    #         permissions of the file's owner rather than the
    #         permissions of the person running the program. If a
    #         file has the SUID bit set, it means that when the
    #         file is executed, it will run with the effective user
    #         ID (UID) of the file's owner.
    #       - When the SUID bit is set on a directory, new files
    #         created within that directory will inherit the group
    #         ownership of the parent directory. This is known as
    #         the "Sticky Bit" behavior on directories.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       has_suid /usr/sbin/mount.cifs
    #   2)
    #       if has_suid /usr/sbin/mount.cifs; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -u "$1" ] && return 0
    return 1
}
#}}}
function has_egid() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and
    #   is owned by the effective group ID of the user
    #
    #   Note:
    #       - The effective group ID is a user attribute that determines
    #         the group ownership of processes and files created by the user.
    #       - When a user runs a process, the process typically inherits the
    #         effective group ID of the user.
    #       - The effective group ID is not necessarily the primary group of
    #         the user; it's the group ID used for determining access
    #         permissions when creating new files or processes.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       has_egid ~/bin
    #   2)
    #       if has_egid ~/.bashrc; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -G "$1" ] && return 0
    return 1
}
#}}}
function has_euid() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists and
    #   is owned by the effective user ID of the user
    #
    #   Note:
    #       - The effective user ID is a user attribute that determines the
    #         ownership of processes and files created by the user.
    #       - When a user runs a process, the process typically inherits the
    #         effective user ID of the user.
    #       - the effective user ID is not necessarily the real user ID or
    #         the user's primary group. It is the user ID used for determining
    #         access permissions when creating new files or processes.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       has_euid ~/bin
    #   2)
    #       if has_euid ~/.bashrc; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -O "$1" ] && return 0
    return 1
}
#}}}
function has_stickybit() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and has its sticky bit set.
    #
    #   Note:
    #       - It's set as `chmod +t filename`.
    #       - The sticky bit is a permission on a directory in Unix-like operating systems.
    #       - When the sticky bit is set on a directory, only the owner of a file within
    #         that directory can delete or rename the file. Other users, even if they have
    #         write permissions on the directory, cannot delete or rename files that do not
    #         belong to them.
    # Params:
    #   1: file
    # Example:
    #   1)
    #       has_stickybit /tmp
    #   2)
    #       if has_stickybit /tmp; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -k "$1" ] && return 0
    return 1
}
#}}}

function are_same_file() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the first and the second given
    #   file/directory have the same device and inode numbers
    # Params:
    #   1: file or directory
    #   2: file or directory
    # Example:
    #   1)
    #       are_same_file ~/file1 ~/file2
    #   2)
    #       if are_same_file ~/file1 ~/file2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ "$1" -ef "$2" ] && return 0
    return 1
}
#}}}
function are_identical_files() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given two files are identical
    # Params:
    #   1: file
    #   2: file
    # Example:
    #   1)
    #       are_identical_files ~/file1 ~/file2
    #   2)
    #       if are_identical_files ~/file1 ~/file2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    cmp -s "$1" "$2" && return 0
    return 1
}
#}}}
function are_identical_directories() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given two directories are identical
    # Params:
    #   1: directory
    #   2: directory
    # Example:
    #   1)
    #       are_identical_directories ~/dir1 ~/dir2
    #   2)
    #       if are_identical_directories ~/dir1 ~/dir2; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    diff -r -q "$1" "$2" > /dev/null 2>&1
    local exit_code=$?

    # Check if diff had no differences (exit code 0)
    if [ $exit_code -eq 0 ]; then
        return 0
    else
        return 1
    fi
}
#}}}

function was_modified() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not a given file exists
    #   and has been modified since it was last read
    # Params:
    #   1: file
    # Example:
    #   1)
    #       was_modified ~/some_file
    #   2)
    #       if was_modified ~/some_file; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -N "$1" ] && return 0
    return 1
}
#}}}


# Strings & Numbers
function is_empty() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string is empty
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a=''
    #       is_empty $a
    #   2)
    #       a=''
    #       if is_empty $a; then
    #           echo 'yes'
    #       else
    #           echo 'no'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ -z "$1" ] && return 0
    return 1
}
#}}}
function is_integer() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string
    #   is a (negative or positive) integer
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a='2'
    #       is_integer $a
    #   2)
    #       a='3'
    #       if is_integer $a; then
    #           echo '$a is integer'
    #       else
    #           echo '$a is not integer'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ "$1" =~ ^-?[0-9]+$ ]] && return 0
    return 1
}
#}}}
function is_decimal() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string
    #   is a (negative or positive) decimal
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a='2'
    #       is_decimal $a
    #   2)
    #       a='3'
    #       if is_decimal $a; then
    #           echo '$a is decimal'
    #       else
    #           echo '$a is not decimal'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ "$1" =~ ^-?[0-9]+\.[0-9]+$ ]] && return 0
    return 1
}
#}}}
function is_numeric() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string
    #   is a (negative or positive) integer or decimal
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a='2'
    #       is_numeric $a
    #   2)
    #       a='3'
    #       if is_numeric $a; then
    #           echo '$a is numeric'
    #       else
    #           echo '$a is not numeric'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ "$1" =~ ^-?[0-9]+(\.[0-9]+)?$ ]] && return 0
    return 1
}
#}}}
function is_scientific() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string
    #   is a (negative or positive) scientific (exponent)
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a='2'
    #       is_scientific $a
    #   2)
    #       a='3'
    #       if is_scientific $a; then
    #           echo '$a is scientific'
    #       else
    #           echo '$a is not scientific'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ "$1" =~ ^-?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)$ ]] && return 0
    return 1
}
#}}}
function is_numeric_or_scientific() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given variable/string
    #   is a (negative or positive) integer or decimal
    #   or scientific
    # Params:
    #   1: variable or string
    # Example:
    #   1)
    #       a='2'
    #       is_numeric_or_scientific $a
    #   2)
    #       a='3'
    #       if is_numeric_or_scientific $a; then
    #           echo '$a is numeric or scientific'
    #       else
    #           echo '$a is not numeric or scientific'
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [[ "$1" =~ ^-?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?$ ]] && return 0
    return 1
}
#}}}


# Arrays
function is_array_empty() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the given array is empty
    # Params:
    #   1: array variable without '$' and ends with [@]
    # Example:
    #   1)
    #       declare -a a=(foo bar)
    #       is_array_empty a[@]
    #   2)
    #       declare -A a=([k1]='foo' [k2]='bar')
    #       if is_array_empty a[@]; then
    #           echo 'array is empty'
    #       else
    #           echo 'array is NOT empty'
    #       fi
    # Return:
    #   0: if true  (array is empty)
    #   1: if false (array is not empty)
    #}}}
    local keys=("${!1}")  # Get the keys of the array

    # method-1: arithmetic expansion
    (( "${#keys[@]}" )) && return 1 # array is not-empty
    return 0 # array is empty

    # method-2:  bash test
    #[ "${#keys[@]}" -gt 0 ] && return 1 # array is not-empty
    #return 0 # array is empty
}
#}}}


# Aliases as functions:
# Files & Directories #{{{
function is_fifo() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: is_pipe()
    #   Check it with: docs_in helper/checks is_pipe
    #}}}
    return $(is_pipe "$1")
}
#}}}
function is_owned() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: has_euid()
    #   Check it with: docs_in helper/checks has_euid
    #}}}
    return $(has_euid "$1")
}
#}}}
function is_dir() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: is_directory()
    #   Check it with: docs_in helper/checks is_directory
    #}}}
    return $(is_directory "$1")
}
#}}}
function is_searchable() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: is_executable()
    #   Check it with: docs_in helper/checks is_executable
    #}}}
    return $(is_executable "$1")
}
#}}}
#}}}
# Strings & Numbers #{{{
function is_float() { #{{{
    # Doc: #{{{
    # About:
    #   Is an alias to: is_decimal()
    #   Check it with: docs_in helper/checks is_decimal
    #}}}
    return $(is_decimal "$1")
}
#}}}
#}}}
