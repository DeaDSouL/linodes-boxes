#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


function ran_as_root_or_with_sudo() { #{{{
    # Doc: #{{{
    # About:
    #   Checks whether or not the script ran as 'root'
    #   or prefixed with 'sudo'
    # Params:
    #   N/A
    # Example:
    #   1)
    #       if ran_as_root_or_with_sudo; then
    #           echo "we have root's powers"
    #       else
    #           echo "we're just a normal user"
    #       fi
    # Return:
    #   0: if true
    #   1: if false
    #}}}
    [ $UID -eq 0 ] && {
        #_log_debug 'ran as root or with sudo'
        return 0
    }
    #_log_debug 'ran as normal user'
    return 1
}
#}}}

function require_normal_user() { #{{{
    # Doc: #{{{
    # About:
    #   Exit if we're not a normal user
    # Params:
    #   1: (Optional) the exit message to be printed
    # Example:
    #   1)
    #       require_normal_user
    #   2)
    #       require_normal_user 'Must be normal user!\nExiting..'
    # Return:
    #   0: if we are not root or wasn't ran with sudo
    # Exit:
    #   1: if we are root, or was ran with sudo
    #}}}
    if ran_as_root_or_with_sudo; then
        echo -e "${1:-'This script must be executed as a normal user.\nAborting..'}"
        exit 1
    fi
    return 0
}
#}}}

function require_root_or_sudo() { #{{{
    # Doc: #{{{
    # About:
    #   Exit if we're not root, or executed without sudo
    # Params:
    #   1: (Optional) the exit message to be printed
    # Example:
    #   1)
    #       require_root_or_sudo
    #   2)
    #       require_root_or_sudo 'Must be root!\nExiting..'
    # Return:
    #   0: if we are root or was ran with sudo
    # Exit:
    #   1: if we aren't root, or wasn't ran with sudo
    #}}}
    if ! ran_as_root_or_with_sudo; then
        echo -e "${1:-'This script must be executed as root or with sudo.\nAborting..'}"
        exit 1
    fi
    return 0
}
#}}}
